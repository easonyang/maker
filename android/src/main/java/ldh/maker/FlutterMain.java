package ldh.maker;

import ldh.maker.component.FlutterContentUiFactory;
import ldh.maker.util.UiUtil;

import static javafx.application.Application.launch;

/**
 * Created by ldh123 on 2018/9/10.
 */
public class FlutterMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new FlutterContentUiFactory());
    }

    protected String getTitle() {
        return "Flutter(android + ios)--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}
