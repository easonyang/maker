<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib  prefix="Template" tagdir="/WEB-INF/tags" %>

<Template:main>
    <jsp:attribute name="headContent">
        <link href="${r'${pageContext.request.contextPath}'}/resource/common/css/pagination.css" rel="stylesheet">
        <script src="${r'${pageContext.request.contextPath}'}/resource/common/js/pagination.js"></script>
    </jsp:attribute>
    <jsp:body>
        <h2>${util.comment(table)}管理</h2>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <#list table.columnList as column>
                    <#if column.create>
                    <th>${util.comment(column)}</th>
                    </#if>
                </#list>
                    <th>操作</th>
                </thead>
                <tbody>
                <c:forEach items="${r'${'}${util.firstLower(table.javaName)}s.beans${r'}'}" var="${util.firstLower(table.javaName)}">
                    <tr>
                    <#list table.columnList as column>
                        <#if column.create>
                        <#if column.foreign>
                        <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'}'}</th>
                        <#elseif util.isDate(column)>
                        <th<#if column.width??> width="${column.width}"</#if>><fmt:formatDate value="${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}" pattern="yyyy-MM-dd HH:mm:ss"/> </th>
                        <#elseif util.isNumber(column)>
                        <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}</th>
                        <#elseif util.isEnum(column)>
                        <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}.desc${r'}'}</th>
                        <#else>
                        <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}</th>
                        </#if>
                        </#if>
                    </#list>
                        <th><a href="/${util.firstLower(table.javaName)}/view/${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}${r'}'}">详情</a>|<a href="/${util.firstLower(table.javaName)}/toEdit/${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}${r'}'}">编辑</a></th>
                    </tr>
                </c:forEach>
                </tbody>
			</table>
            <Template:pagination  formId="${util.firstLower(table.javaName)}Form"
                                  pageResult="${r'${'}${util.firstLower(table.javaName)}s${r'}'}"
                                  pageView="10"
                                  action="${r'${pageContext.request.contextPath}'}/${util.firstLower(table.javaName)}/list">
            </Template:pagination>
		</div>
    </jsp:body>
</Template:main>