<%@ tag language="java" pageEncoding="utf-8"%>
<%@ attribute name="headContent" fragment="true" required="false" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>代码自动生成测试平台</title>
    <link href="${r'${pageContext.request.contextPath}'}/resource/frame/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="${r'${pageContext.request.contextPath}'}/resource/frame/bootstrap/css/dashboard.css" rel="stylesheet">

    <jsp:invoke fragment="headContent"/>
</head>

<body>
    <jsp:include page="/WEB-INF/jsp/common/head.jsp"/>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <jsp:include page="/WEB-INF/jsp/common/left.jsp"/>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1>Dashboard</h1>
                </div>
                <jsp:doBody/>
            </main>
        </div>
    </div>

    <script src="${r'${pageContext.request.contextPath}'}/resource/common/js/jquery-1.8.0.min.js"></script>
    <script src="${r'${pageContext.request.contextPath}'}/resource/frame/bootstrap/js/bootstrap.js"></script>
</body>
</html>
