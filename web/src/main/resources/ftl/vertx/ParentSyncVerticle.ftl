package ${projectPackage}.verticle;

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sync.SyncVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.TemplateEngine;
import ${projectPackage}.util.VertxUtil;

public abstract class ParentSyncVerticle extends SyncVerticle {

    protected Router router;
    protected TemplateEngine engine;

    @Override
    public void start() throws Exception {
        this.router = VertxUtil.getRouter();
        engine = VertxUtil.getTemplateEngine();
        buildRouter();
    }

    protected void render(RoutingContext ctx, String dir, String template) {
        engine.render(ctx, dir, template, res -> {
            ctx.response().putHeader("content-type", "text/html;charset=UTF-8");
            if (res.succeeded()) {
                ctx.response().end(res.result());
            } else {
                ctx.fail(res.cause());
            }
        });
    }

    protected void jsonOut(RoutingContext ctx, boolean isSuccess, String json) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("status", isSuccess);
        jsonObject.put("msg", json);
        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(jsonObject));
    }

    public abstract void buildRouter();

}
