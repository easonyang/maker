package ${beanPackage};

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

public class DbUtil {

    private static volatile JDBCClient jdbcClient = null;

    public static JDBCClient getJdbcClient(Vertx vertx) {
        if (jdbcClient == null) {
            synchronized (DbUtil.class) {
                if (jdbcClient == null) {
                    jdbcClient = JDBCClient.createShared(vertx, new JsonObject()
                            .put("url", "jdbc:mysql://${jdbc_ip}:${jdbc_port?c}/${jdbc_dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false")
                            .put("driver_class", "com.mysql.jdbc.Driver")
                            .put("user", "${jdbc_userName}")
                            .put("password", "${jdbc_password}")
                            .put("max_pool_size", 200)
                            .put("castUUID", true));
                }
            }
        }
        return jdbcClient;
    }
}
