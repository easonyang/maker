package ${beanPackage};

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import co.paralleluniverse.fibers.Suspendable;
import io.vertx.core.MultiMap;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.RoutingContext;
import ldh.common.PageResult;
import ${servicePackage}.${table.javaName}Service;
import ${projectPackage}.util.BeanUtil;
import ${projectPackage}.util.DbUtil;
import ${projectPackage}.util.JsonObjectUtil;
import ${projectPackage}.util.VertxUtil;
import ${projectPackage}.verticle.ParentSyncVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static io.vertx.ext.sync.Sync.awaitResult;
import static io.vertx.ext.sync.Sync.fiberHandler;

public class ${table.javaName}Router extends ParentSyncVerticle {

	private static final Logger logger = LoggerFactory.getLogger(${table.javaName}Router.class);

	private ${table.javaName}Service ${util.firstLower(table.javaName)}Service = BeanUtil.getBean(${table.javaName}Service.class);

	public void buildRouter() {
		router.get("/${util.firstLower(table.javaName)}/list").handler(fiberHandler(this::list));
		router.get("/${util.firstLower(table.javaName)}/list/json").handler(fiberHandler(this::listJson));
		router.get("/${util.firstLower(table.javaName)}/view/:id").handler(fiberHandler(this::view));
		router.get("/${util.firstLower(table.javaName)}/view/json/:id").handler(fiberHandler(this::viewJson));
		router.get("/${util.firstLower(table.javaName)}/toEdit/:id").handler(fiberHandler(this::toEdit));
		router.get("/${util.firstLower(table.javaName)}/save/json").handler(fiberHandler(this::saveJson));
		router.get("/${util.firstLower(table.javaName)}/save").handler(fiberHandler(this::save));
	}

	@Suspendable
	private void list(RoutingContext ctx) {
		JDBCClient jdbcClient = DbUtil.getJdbcClient(vertx);
		try {
			JsonObject jsonObject = new JsonObject();
			jsonObject.put("pageNo", ctx.request().getParam("pageNo"));
			jsonObject.put("pageSize", ctx.request().getParam("pageSize"));
			PageResult<JsonObject> ${util.firstLower(table.javaName)}s = ${util.firstLower(table.javaName)}Service.list(jdbcClient, jsonObject);
    		ctx.put("${util.firstLower(table.javaName)}s", ${util.firstLower(table.javaName)}s);
    		render(ctx, "/template/", "${util.firstLower(table.javaName)}/${util.firstLower(table.javaName)}List.ftl");
		} catch (Exception e) {
			logger.error("list", e);
    		render(ctx, "/template/", "error.ftl");
		}
	}

    @Suspendable
    private void listJson(RoutingContext ctx) {
    	JDBCClient jdbcClient = DbUtil.getJdbcClient(vertx);
    	try {
			JsonObject jsonObject = new JsonObject();
			jsonObject.put("pageNo", ctx.request().getParam("pageNo"));
			jsonObject.put("pageSize", ctx.request().getParam("pageSize"));
			PageResult<JsonObject> ${util.firstLower(table.javaName)}s = ${util.firstLower(table.javaName)}Service.list(jdbcClient, jsonObject);
        	jsonOut(ctx, true, Json.encodePrettily(${util.firstLower(table.javaName)}s));
        } catch (Exception e) {
        	logger.error("listJson", e);
        	jsonOut(ctx, false, "list json error");
        }
    }

	@Suspendable
	private void view(RoutingContext ctx) {
		JDBCClient jdbcClient = DbUtil.getJdbcClient(vertx);
		try {
			JsonObject jsonObject = new JsonObject();
			jsonObject.put("${util.firstLower(table.primaryKey.column.name)}", ctx.request().getParam("${util.firstLower(table.primaryKey.column.name)}"));
			JsonObject ${util.firstLower(table.javaName)} = ${util.firstLower(table.javaName)}Service.getBy${util.firstUpper(table.primaryKey.column.name)}(jdbcClient, jsonObject);
        	ctx.put("${util.firstLower(table.javaName)}", ${util.firstLower(table.javaName)});
        	render(ctx, "/template/", "${util.firstLower(table.javaName)}/${util.firstLower(table.javaName)}View.ftl");
		} catch (Exception e) {
			logger.error("view", e);
        	render(ctx, "/template/", "error.ftl");
		}
	}

	@Suspendable
	private void viewJson(RoutingContext ctx) {
		JDBCClient jdbcClient = DbUtil.getJdbcClient(vertx);
		try {
			JsonObject jsonObject = new JsonObject();
			jsonObject.put("${util.firstLower(table.primaryKey.column.name)}", ctx.request().getParam("${util.firstLower(table.primaryKey.column.name)}"));
			JsonObject ${util.firstLower(table.javaName)} = ${util.firstLower(table.javaName)}Service.getBy${util.firstUpper(table.primaryKey.column.name)}(jdbcClient, jsonObject);
			jsonOut(ctx, true, Json.encodePrettily(${util.firstLower(table.javaName)}));
		} catch (Exception e) {
			logger.error("listJson", e);
			jsonOut(ctx, false, "view json error");
		}
	}

	@Suspendable
	private void toEdit(RoutingContext ctx) {
        JDBCClient jdbcClient = DbUtil.getJdbcClient(vertx);
		try {
        	JsonObject jsonObject = new JsonObject();
        	jsonObject.put("${util.firstLower(table.primaryKey.column.name)}", ctx.request().getParam("${util.firstLower(table.primaryKey.column.name)}"));
        	JsonObject ${util.firstLower(table.javaName)} = ${util.firstLower(table.javaName)}Service.getBy${util.firstUpper(table.primaryKey.column.name)}(jdbcClient, jsonObject);
        	ctx.put("${util.firstLower(table.javaName)}", ${util.firstLower(table.javaName)});
			render(ctx, "/template/", "${util.firstLower(table.javaName)}/${util.firstLower(table.javaName)}Edit.ftl");
		} catch (Exception e) {
			logger.error("listJson", e);
        	render(ctx, "/template/", "error.ftl");
		}
	}

	@Suspendable
	private void save(RoutingContext ctx) {
		JDBCClient jdbcClient = DbUtil.getJdbcClient(vertx);
		try {
        	JsonObject jsonObject = JsonObjectUtil.toJsonObject(ctx.request().params());
			${util.firstLower(table.javaName)}Service.save(jdbcClient, jsonObject);
        	ctx.reroute("/${util.firstLower(table.javaName)}/list");
		} catch (Exception e) {
			logger.error("view", e);
			render(ctx, "/template/", "error.ftl");
		}
	}

        @Suspendable
        private void saveJson(RoutingContext ctx) {
        	JDBCClient jdbcClient = DbUtil.getJdbcClient(vertx);
			try {
        		JsonObject jsonObject = JsonObjectUtil.toJsonObject(ctx.request().params());
				${util.firstLower(table.javaName)}Service.save(jdbcClient, jsonObject);
				jsonOut(ctx, true, "success");
			} catch (Exception e) {
				logger.error("listJson", e);
				jsonOut(ctx, false, "save json error");
			}
        }

}