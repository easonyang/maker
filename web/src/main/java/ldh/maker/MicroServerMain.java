package ldh.maker;

import ldh.maker.ServerMain;
import ldh.maker.component.VertxContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh123 on 2018/6/23.
 */
public class MicroServerMain extends ServerMain {

    @Override
    protected void preHandle() {
    }

    @Override
    protected String getTitle() {
        return "微服务端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}
