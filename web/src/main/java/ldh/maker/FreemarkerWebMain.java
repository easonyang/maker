package ldh.maker;

import ldh.maker.component.BootstrapContentUiFactory;
import ldh.maker.component.FreemarkerContentUiFactory;
import ldh.maker.util.UiUtil;

public class FreemarkerWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new FreemarkerContentUiFactory());
    }

    @Override
    protected String getTitle() {
        return "Freemarker前后端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}

