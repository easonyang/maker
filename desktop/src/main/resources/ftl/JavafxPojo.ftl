package ${package};

<#if imports?exists>
    <#list imports as import>
import ${import};
    </#list>
</#if>

<#if table.comment??>
/**
* ${table.comment}
*/
</#if>
<#if serializable>@SuppressWarnings("serial")</#if>
public class ${className} <#if extend?exists>extends ${extend}</#if> <#if implements?exists>implements <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if> ${r'{'}

<#if table.columnList??>
    <#list table.columnList as column>
        <#if !column.foreign>
            <#if column.comment??>

    /**
    * ${column.comment}
    */
        </#if>
        <#if util.isJavafxProperty(column)>
    private ${column.javaType}Property ${column.property} = new Simple${column.javaType}Property();
        <#else>
    private ObjectProperty<${column.javaType}> ${column.property} = new SimpleObjectProperty();
        </#if>

        </#if>
    </#list>
</#if>

<#if table.foreignKeys??>
    <#list table.foreignKeys as foreignKey>
        <#if foreignKey.column.comment??>
    /**
    * ${foreignKey.column.comment}
    */
    </#if>
    private ObjectProperty<${util.firstUpper(foreignKey.foreignTable.javaName)}> ${foreignKey.column.property} = new SimpleObjectProperty();
    </#list>
</#if>

<#-- 一对多中多的对象 -->
<#if table.columnList??>
    <#list table.many as foreignKey>
        <#if foreignKey.table.create>
            <#if foreignKey.oneToOne>
    public ObjectProperty<${util.firstUpper(foreignKey.table.javaName)}> ${util.firstLower(foreignKey.table.javaName)} = new SimpleObjectProperty();

    <#else>
    public ListProperty<${util.firstUpper(foreignKey.table.javaName)}> ${util.firstLower(foreignKey.table.javaName)}s = new SimpleListProperty<>();

            </#if>
        </#if>
    </#list>
</#if>
<#-- 多对多关系 -->
<#if table.columnList?? && table.manyToManys??>
    <#list table.manyToManys as manyToMany>
    //多对多关系
    public ListProperty<${util.firstUpper(manyToMany.secondTable.javaName)}> ${util.firstLower(manyToMany.secondTable.javaName)}s = new SimpleListProperty<>();

    </#list>
</#if>
<#-- ... -->
<#if table.columnList??>
    <#list table.columnList as column>
        <#if !column.foreign>
    public void set${util.upFirst(column.property)}(${column.javaType} ${column.property}) {
        this.${column.property}.set(${column.property});
    }

    public ${column.javaType} get${util.upFirst(column.property)}() {
        return ${column.property}.get();
    }

        <#if util.isJavafxProperty(column)>
    public ${column.javaType}Property ${column.property}Property(){
        return ${column.property};
    }

        <#else>
    public ObjectProperty<${column.javaType}> ${column.property}Property() {
        return ${column.property};
    }

        </#if>
        </#if>
    </#list>
</#if>
<#-- 外键-->
<#if table.foreignKeys??>
    <#list table.foreignKeys as foreignColumn>
    public void set${util.firstUpper(foreignColumn.foreignTable.javaName)}(${util.firstUpper(foreignColumn.foreignTable.javaName)} ${foreignColumn.column.property}) {
        this.${foreignColumn.column.property}.set(${foreignColumn.column.property});
    }

    public ${util.firstUpper(foreignColumn.foreignTable.javaName)} get${util.firstUpper(foreignColumn.column.property)}() {
        return ${foreignColumn.column.property}.get();
    }

    public ObjectProperty<${util.firstUpper(foreignColumn.foreignTable.javaName)}> ${foreignColumn.column.property}Property() {
        return ${foreignColumn.column.property};
    }

    </#list>
</#if>
<#-- 一对多中多的对象 -->
<#if table.columnList?? && table.many??>
    <#list table.many as foreignKey>
        <#if foreignKey.table.create>
            <#if !foreignKey.oneToOne>
            <#-- 外键设置-->
                <#if foreignKey??>
    public void set${util.firstUpper(foreignKey.table.javaName)}s(ObservableList<${util.firstUpper(foreignKey.table.javaName)}> ${util.firstLower(foreignKey.table.javaName)}s) {
        this.${util.firstLower(foreignKey.table.javaName)}s.set(${util.firstLower(foreignKey.table.javaName)}s);
    }

    public ListProperty<${util.firstUpper(foreignKey.table.javaName)}> ${util.firstLower(foreignKey.table.javaName)}sProperty() {
        return this.${util.firstLower(foreignKey.table.javaName)}s;
    }

    public ObservableList<${util.firstUpper(foreignKey.table.javaName)}> get${util.firstUpper(foreignKey.table.javaName)}s() {
        return this.${util.firstLower(foreignKey.table.javaName)}s.get();
    }
                </#if>
            </#if>
        </#if>
    </#list>
<#-- 多对多关系 -->
    <#if table.columnList?? && table.manyToManys??>
        <#list table.manyToManys as manyToMany>
            //多对多关系
    public void set${util.firstUpper(manyToMany.secondTable.javaName)}s(ObservableList<${util.firstUpper(manyToMany.secondTable.javaName)}> ${util.firstLower(manyToMany.secondTable.javaName)}s) {
        this.${util.firstLower(manyToMany.secondTable.javaName)}s.set(${util.firstLower(manyToMany.secondTable.javaName)}s);
    }

    public ListProperty<${util.firstUpper(manyToMany.secondTable.javaName)}> ${util.firstLower(manyToMany.secondTable.javaName)}sProperty() {
        return this.${util.firstLower(manyToMany.secondTable.javaName)}s;
    }

    public ObservableList<${util.firstUpper(manyToMany.secondTable.javaName)}> get${util.firstUpper(manyToMany.secondTable.javaName)}s() {
        return this.${util.firstLower(manyToMany.secondTable.javaName)}s.get();
    }
        </#list>
    </#if>
</#if>
${r'}'}