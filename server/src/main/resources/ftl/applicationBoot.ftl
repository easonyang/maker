package ${projectRootPackage};

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class ApplicationBoot extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationaClass);
    }

    private static Class<ApplicationBoot> applicationaClass = ApplicationBoot.class;

    public static void main(String[] args) {
        SpringApplication.run(ApplicationBoot.class, args);
    }
}
