<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${namespace}">

	<#-------------------------------------getByUnique-------------------------------------------------------------------------------->
	<#list table.indexies as index>
	<#if util.isCreate(table, "getBy" + util.uniqueName(index)) >
	<!-- 通过唯一索引单表查询 -->
	<select id="getBy${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${util.uniqueType(index)}"</#if> resultMap="${util.firstLower(beanName)}Mapper">
		select <include refid="selectColumns"/>
		from ${table.name} ${alias}
		<#if index.columns?size = 1>
		where ${aliasQueryPrefix}${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
		<#list index.columns as column>
			AND ${aliasQueryPrefix}${column.name} = ${r"#{"}${column.property}${r"}"}
		</#list>
		</where>
		</#if>
	</select>
	</#if>

	<#------------------------------------getWith------------------------------------------------------------------------------>
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#list table.foreignKeys as foreignKey>
	<#if util.isCreate(table, "getWith${util.firstUpper(foreignKey.column.property)}By"  + util.uniqueName(index)) && !foreignKey.table.middle>
	<!-- 通过唯一索引查询， 并查询${util.firstUpper(foreignKey.column.property)}数据 -->
	<select id="getWith${util.firstUpper(foreignKey.column.property)}By${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectWith${util.firstUpper(foreignKey.column.property)}Mapper">
		select <include refid="selectColumns"/>
		from ${table.name} ${alias}
		<#if index.columns?size = 1>
		where ${aliasQueryPrefix}${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
		<#list index.columns as column>
			AND ${aliasQueryPrefix}${column.name} = ${r"#{"}${column.property}${r"}"}
		</#list>
		</where>
		</#if>
	</select>

	</#if>
	</#list>
	</#if>
	<#------------------------------------getWithAssociatesById---------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getWithAssociatesBy"  + util.uniqueName(index))>
	<!-- 通过唯一索引查询， 并查询管理的所有多对一的一方数据 -->
	<select id="getWithAssociatesBy${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectWithAssociatesMapper">
		select <include refid="selectColumns"/>
		from ${table.name} ${alias}
		<#if index.columns?size = 1>
		where ${aliasQueryPrefix}${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
		<#list index.columns as column>
			AND ${aliasQueryPrefix}${column.name} = ${r"#{"}${column.property}${r"}"}
		</#list>
		</where>
		</#if>
	</select>

	</#if>
	</#if>
	<#------------------------------------getJoin------------------------------------------------------------------------------>
	<#list table.foreignKeys as foreignKey>
	<#if util.isCreate(table, "getJoin${util.firstUpper(foreignKey.column.property)}By"  + util.uniqueName(index))>
	<#if foreignKey.foreignTableName != table.name>
	<!-- 通过唯一索引查询， 关联查询${util.firstUpper(foreignKey.column.property)}数据 -->
	<select id="getJoin${util.firstUpper(foreignKey.column.property)}By${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectJoin${util.firstUpper(foreignKey.column.property)}Mapper">
		select <include refid="selectColumns"/>, <include refid="${baseNamespace}.${util.firstUpper(foreignKey.foreignTable.javaName)}Dao.selectColumns"/> from ${table.name} ${alias}
		left join ${foreignKey.foreignTableName} ${foreignKey.foreignTable.alias} on ${alias}.${foreignKey.column.name} = ${foreignKey.foreignTable.alias}.${foreignKey.foreignColumn.name}
		<#if index.columns?size = 1>
		where ${alias}.${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
			<#list index.columns as column>
		AND ${alias}.${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</select>

	</#if>
	</#if>
	</#list>
	<#------------------------------------getJoinAssociatesById---------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getJoinAssociatesBy" + util.uniqueName(index))>
	<!-- 通过唯一索引查询， 关联查询所有多对一中一方的数据 -->
	<select id="getJoinAssociatesBy${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectJoinAssociatesMapper">
		select <include refid="selectColumns"/> <#list table.foreignKeys as foreignKey><#if foreignKey.foreignTableName != table.name && foreignKey.foreignTable.create>, <include refid="${baseNamespace}.${util.firstUpper(foreignKey.foreignTable.javaName)}Dao.selectColumns"/></#if></#list>
		from ${table.name} ${alias}
		<#list table.foreignKeys as foreignKey>
			<#if foreignKey.foreignTableName != table.name && foreignKey.foreignTable.create>
		left join ${foreignKey.foreignTableName} ${foreignKey.foreignTable.alias} on ${alias}.${foreignKey.column.name} = ${foreignKey.foreignTable.alias}.${foreignKey.foreignColumn.name}
			</#if>
		</#list>
		<#if index.columns?size = 01>
		where ${alias}.${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
			<#list index.columns as column>
		AND ${alias}.${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</select>

	</#if>
	</#if>
	<#--------------getManyJoin-------------------------------------------------------------------------------------------->
	<#if table.many?? && table.many?size gt 0 >
	<#list table.many as manyOne>
	<#if util.isCreate(table, "getJoin${util.firstUpper(manyOne.table.javaName)}By"+ util.uniqueName(index)) >
	<!-- 通过唯一索引查询， 关联查询多对一中多方${util.firstUpper(manyOne.table.javaName)}数据 -->
	<select id="getJoin${util.firstUpper(manyOne.table.javaName)}By${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectJoin${util.firstUpper(manyOne.table.javaName)}Mapper">
		select <include refid="selectColumns"/>, <include refid="${baseNamespace}.${util.firstUpper(manyOne.table.javaName)}Dao.selectColumns"/>
		from ${table.name} ${alias}
		left join ${manyOne.table.name} ${manyOne.table.alias} on ${alias}.${manyOne.column.name} = ${manyOne.table.alias}.${manyOne.foreignColumn.name}
		<#if index.columns?size = 1>
		where ${alias}.${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
			<#list index.columns as column>
			AND ${alias}.${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</select>
	</#if>
	</#list>
	</#if>
	<#--------------getManyJoinAllById------------------------------------------------------------------------------------->
	<#if table.many?size gt 1>
	<#if util.isCreate(table, "getJoinAllBy"+ util.uniqueName(index)) >
	<!-- 通过唯一索引查询， 关联查询所有多对一中多方数据 -->
	<select id="getJoinAllBy${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectJoinAllMapper">
		select <include refid="selectColumns"/><#list table.many as manyOne><#if manyOne.table.name != table.name && manyOne.table.create>,<include refid="${baseNamespace}.${util.firstUpper(manyOne.table.javaName)}Dao.selectColumns"/></#if></#list>
		from ${table.name} ${alias}
		<#list table.many as manyOne>
			<#if manyOne.table.name != table.name && manyOne.table.create>
				left join ${manyOne.table.name} ${manyOne.table.alias} on ${alias}.${manyOne.foreignColumn.name} = ${manyOne.table.alias}.${manyOne.column.name}
			</#if>
		</#list>
		<#if index.columns?size = 1>
		where ${alias}.${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
			<#list index.columns as column>
			AND ${alias}.${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</select>
	</#if>
	</#if>
	<#------------------------------------getManyToManys------------------------------------------------------------------------>
	<#if table.manyToManys?? && table.manyToManys?size gt 0 >
	<#list table.manyToManys as mm>
	<#if util.isCreate(mm.primaryTable, "getMany" + util.firstUpper(mm.secondTable.javaName) + "sBy" + util.uniqueName(index))>
	<!-- 通过唯一索引查询， 关联查询多对多${util.firstUpper(mm.secondTable.javaName)}数据 -->
	<select id="getMany${util.firstUpper(mm.secondTable.javaName)}sBy${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectMany${util.firstUpper(mm.secondTable.javaName)}sMapper">
		select <include refid="selectColumns"/>, <include refid="${baseNamespace}.${util.firstUpper(mm.secondTable.javaName)}Dao.selectColumns"/>
		from ${mm.primaryTable.name} ${alias}
		left join ${mm.middleTable.name} ${mm.middleTable.alias} on ${alias}.${mm.primaryTable.primaryKey.column.name} = ${mm.middleTable.alias}.${mm.primaryForeignKey.column.name}
		left join ${mm.secondTable.name} ${mm.secondTable.alias} on ${mm.secondTable.alias}.${mm.secondTable.primaryKey.column.name} = ${mm.middleTable.alias}.${mm.secondForeignKey.column.name}
		<#if index.columns?size = 1>
		where ${alias}.${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
			<#list index.columns as column>
			AND ${alias}.${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</select>

	</#if>
	</#list>
	</#if>
	<#------------------------------------getManyJoinAllById--------------------------------------------------------------------->
	<#if table.manyToManys?? && table.manyToManys?size gt 1 >
	<#if util.isCreate(table, "getManyJoinAllBy" + util.uniqueName(index))>
	<!-- 通过唯一索引查询， 关联查询所有多对多数据 -->
	<select id="getManyJoinAllBy${util.uniqueName(index)}" <#if index.columns?size = 1>parameterType="${key}"</#if> resultMap="selectManyJoinAllMapper">
		select <include refid="selectColumns"/><#list table.manyToManys as mtm>,<include refid="${baseNamespace}.${util.firstUpper(mtm.secondTable.javaName)}Dao.selectColumns"/></#list>
		from ${table.name} ${alias}
		<#list table.manyToManys as mtm>
		left join ${mtm.middleTable.name} ${mtm.middleTable.alias} on ${alias}.${mtm.primaryForeignKey.foreignColumn.name} = ${mtm.middleTable.alias}.${mtm.primaryForeignKey.column.name}
		left join ${mtm.secondTable.name} ${mtm.secondTable.alias} on ${mtm.middleTable.alias}.${mtm.secondForeignKey.column.name} = ${mtm.secondTable.alias}.${mtm.secondForeignKey.foreignColumn.name}
		</#list>
		<#if index.columns?size = 1>
		where ${alias}.${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
			<#list index.columns as column>
			AND ${alias}.${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</select>

	</#if>
	</#if>
	</#list>
	<#------------------------------------insert------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insert")>
	<insert id="insert" useGeneratedKeys="true" keyProperty="${table.primaryKey.column.property}" parameterType="${util.type(beanName)}">
		insert into ${table.name}(
			<#list table.columnList as column>
			${column.name}<#if column_has_next>,</#if>
			</#list>
		)values(
			<#list table.columnList as column>
			<#if util.containsKey(typeHandlers, column)>
			<#if column.foreign>
			${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
			<#else>
			${r"#{"}${column.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
			</#if>
			<#else>
			<#if column.foreign>
			${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
			<#else>
			${r"#{"}${column.property}${r"}"}<#if column_has_next>,</#if>
			</#if>
			</#if>
			</#list>
		)
	</insert>
	</#if>
	<#------------------------------------insertSelective------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insertSelective")>
	<insert id="insertSelective" useGeneratedKeys="true" keyProperty="${table.primaryKey.column.property}" parameterType="${util.type(beanName)}">
		insert into ${table.name}
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<#list table.columnList as column>
			<if test="${column.property} != null">
				${column.name}<#if column_has_next>,</#if>
			</if>
			</#list>
		</trim>
		<trim prefix="values (" suffix=")" suffixOverrides=",">
			<#list table.columnList as column>
			<if test="${column.property} != null">
				<#if util.containsKey(typeHandlers, column)>
				<#if column.foreign>
				${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
				<#else>
				${r"#{"}${column.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
				</#if>
				<#else>
				<#if column.foreign>
				${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
				<#else>
				${r"#{"}${column.property}${r"}"}<#if column_has_next>,</#if>
				</#if>
				</#if>
			</if>
			</#list>
		</trim>
	</insert>
	</#if>

	<#------------------------------------update------------------------------------------------------------------------------->
	<#list table.indexies as index>
	<#if util.isCreate(table, "updateBy" + util.uniqueName(index))>
	<!-- 更新所有数据 -->
	<update id="updateBy${util.uniqueName(index)}" parameterType="${util.type(beanName)}">
		update ${table.name} set
			<#list table.columnList as column>
			<#if util.containsKey(typeHandlers, column)>
			<#if column.foreign>
			${column.name} = ${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
			<#else>
			${column.name} = ${r"#{"}${column.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
			</#if>
			<#else>
			<#if column.foreign>
			${column.name} = ${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}${r"}"}<#if column_has_next>,</#if>
			<#elseif table.primaryKey.column.name != column.name>
			${column.name} = ${r"#{"}${column.property}${r"}"}<#if column_has_next>,</#if>
			</#if>
			</#if>
			</#list>
		<#if index.columns?size = 1>
		<#if index.columns[0].oneToOne>
		where ${index.columns[0].name} = ${util.type(beanName)}.get${r"#{"}${util.firstUpper(index.columns[0].property)}${r"}"}().getId()
		<#else>
		where ${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		</#if>
		<#else>
		<where>
			<#list index.columns as column>
			AND ${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</update>
	</#if>

	<#------------------------------------updateNotnull-------------------------------------------------------------------------->
	<#if util.isCreate(table, "updateNotNullBy" + util.uniqueName(index)) && !table.primaryKey.composite>
	<!-- 更新非空数据 -->
	<update id="updateNotNullBy${util.uniqueName(index)}" parameterType="${util.type(beanName)}">
		update ${table.name}
		<set>
			<#list table.columnList as column>
			<#if util.containsKey(typeHandlers, column)>
			<if test="${column.property} != null">
				<#if column.foreign>
				${column.name} = ${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
				<#else>
				${column.name} = ${r"#{"}${column.property}, jdbcType=${column.type}${r"}"}<#if column_has_next>,</#if>
				</#if>
			</if>
			<#elseif table.primaryKey.column.name !=column.name>
			<if test="${column.property} != null">
				<#if column.foreign>
				${column.name} = ${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}${r"}"}<#if column_has_next>,</#if>
				<#else>
				${column.name} = ${r"#{"}${column.property}${r"}"}<#if column_has_next>,</#if>
				</#if>
			</if>
			</#if>
			</#list>
		</set>
		<#if index.columns?size = 1>
			<#if index.columns[0].oneToOne>
		where ${index.columns[0].name} = ${util.type(beanName)}.get${r"#{"}${util.firstUpper(index.columns[0].property)}${r"}"}().getId()
			<#else>
		where ${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
			</#if>
		<#else>
		<where>
			<#list index.columns as column>
			AND ${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</update>

	</#if>
	<#------------------------------------delete------------------------------------------------------------------------------>
	<#if util.isCreate(table, "deleteBy" + util.uniqueName(index)) >
	<#if index.columns?size = 1>
	<delete id="deleteBy${util.uniqueName(index)}" parameterType="${index.columns[0].javaType}">
		delete from ${table.name} where ${index.columns[0].name} = ${r"#{id}"}
	</delete>
	<#else>
	<delete id="deleteBy${util.uniqueName(index)}">
		delete from ${table.name}
		<#if index.columns?size = 1>
		where ${index.columns[0].name} = ${r"#{"}${index.columns[0].property}${r"}"}
		<#else>
		<where>
			<#list index.columns as column>
			AND ${column.name} = ${r"#{"}${column.property}${r"}"}
			</#list>
		</where>
		</#if>
	</delete>
	</#if>
	</#if>
	</#list>
	<#------------------------------------find--------------------------------------------------------------------------------->
	<#if util.isCreate(table, "find") >
	<!-- 单表分页查询 -->
	<select id="findBy${beanExtend}" parameterType="${util.type(beanExtend)}" resultMap="${util.firstLower(beanName)}Mapper">
		<#if util.isMysql()>
		select <include refid="selectColumns"/> from ${table.name} ${alias}
		<where>
			<include refid="defaultDyamicWhere"></include>
		</where>
		<if test="start != null and length != null" >
		limit ${r"#{start}"}, ${r"#{length}"}
		</if>
		<#elseif util.isOracle()>
		select * from (
			select <include refid="selectColumns"/>, ROWNUM RN
			from (
				select * from ${table.name} ${alias}
				<where>
					<include refid="defaultDyamicWhere"></include>
				</where>
			) ${alias} where ROWNUM &lt;= ${r"#{end}"}
		) WHERE RN &gt; ${r"#{start}"}
		</#if>
	</select>

	</#if>
	<#------------------------------------findTotal----------------------------------------------------------------------------->
	<#if util.isCreate(table, "find") >
	<select id="findTotalBy${beanExtend}" parameterType="${util.type(beanExtend)}" resultType="Long">
		select count(*) from ${table.name} ${alias}
		<where>
			<include refid="defaultDyamicWhere"></include>
		</where>
	</select>

	</#if>
	<#------------------------------------findByJoin----------------------------------------------------------------------------->
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if util.isCreate(table, "findJoinBy") >
	<!-- 单表关联查询（多对一） -->
	<select id="findJoinBy${beanExtend}" parameterType="${util.type(beanExtend)}" resultMap="selectJoinAssociatesMapper">
		<#if util.isMysql()>
		select <include refid="selectColumns"/> <#list table.foreignKeys as foreignKey><#if foreignKey.foreignTableName != table.name && foreignKey.foreignTable.create> , <include refid="${baseNamespace}.${util.firstUpper(foreignKey.foreignTable.javaName)}Dao.selectColumns"/></#if></#list>
		from ${table.name} ${alias}
		<#list table.foreignKeys as foreignKey>
		<#if foreignKey.foreignTableName != table.name && foreignKey.foreignTable.create>
		left join ${foreignKey.foreignTableName} ${foreignKey.foreignTable.alias} on ${alias}.${foreignKey.column.name} = ${foreignKey.foreignTable.alias}.${foreignKey.foreignColumn.name}
		</#if>
		</#list>
		<where>
			<include refid="defaultDyamicWhere"></include>
			<include refid="joinLeftWhere"></include>
		</where>
		<if test="start != null and length != null" >
		limit ${r"#{start}"}, ${r"#{length}"}
		</if>
		<#elseif  util.isOracle()>
		select * from (
			select t.*, ROWNUM RN
			from (
				select <include refid="selectColumns"/> <#list table.foreignKeys as foreignKey><#if foreignKey.foreignTableName != table.name && foreignKey.foreignTable.create>, <include refid="${baseNamespace}.${util.firstUpper(foreignKey.foreignTable.javaName)}Dao.selectColumns"/></#if></#list>
				from ${table.name} ${alias}
				<#list table.foreignKeys as foreignKey>
				<#if foreignKey.foreignTableName != table.name && foreignKey.foreignTable.create>
				left join ${foreignKey.foreignTableName} ${foreignKey.foreignTable.alias} on ${alias}.${foreignKey.column.name} = ${foreignKey.foreignTable.alias}.${foreignKey.foreignColumn.name}
				</#if>
				</#list>
				<where>
					<include refid="defaultDyamicWhere"></include>
					<include refid="joinLeftWhere"></include>
				</where>
			) t where ROWNUM &lt;= ${r"#{end}"}
		) WHERE RN &gt; ${r"#{start}"}
		</#if>
	</select>

	</#if>
	</#if>
	<#------------------------------------findTotalByJoin--------------------------------------------------------------------->
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if util.isCreate(table, "findJoinBy") >
	<select id="findJoinTotalBy${beanExtend}" parameterType="${util.type(beanExtend)}" resultType="Long">
		select count(*)
		from ${table.name} ${alias}
		<#list table.foreignKeys as foreignKey>
		<#if foreignKey.foreignTableName != table.name && foreignKey.foreignTable.create>
		left join ${foreignKey.foreignTableName} ${foreignKey.foreignTable.alias} on ${alias}.${foreignKey.column.name} = ${foreignKey.foreignTable.alias}.${foreignKey.foreignColumn.name}
		</#if>
		</#list>
	<where>
		<include refid="defaultDyamicWhere"></include>
		<include refid="joinLeftWhere"></include>
	</where>
	</select>

	</#if>
	</#if>
	<#------------------------------------mapper----------------------------------------------------------------------------->
	<#-------------------------------------有外键------------------------------------------------------------------------------->
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0>
	<resultMap id="baseMapper" type="${util.type(beanName)}">
		<#list table.columnList as column>
		<#if !column.foreignKey??>
		<#if table.primaryKey.column?exists && table.primaryKey.column.name == column.name>
		<id property="${column.property}" column="${aliasColumnPrefix}${column.name}"/>
		<#else>
		<result property="${column.property}" column="${aliasColumnPrefix}${column.name}"/>
		</#if>
		</#if>
		</#list>
	</resultMap>

	<resultMap id="${util.firstLower(beanName)}Mapper" type="${util.type(beanName)}" extends="baseMapper">
		<#list table.columnList as column>
		<#if column.foreignKey?? && column.foreignKey.foreignTable.create>
		<association property="${column.property}" javaType="${util.type(util.firstUpper(column.foreignKey.foreignTable.javaName))}">
			<id property="${column.foreignKey.foreignColumn.property}" column="${aliasColumnPrefix}${column.name}"/>
	  	</association>
		</#if>
		</#list>
	</resultMap>

	</#if>
	<#-------------------------------------无外键------------------------------------------------------------------------------->
	<#if table.foreignKeys?? && table.foreignKeys?size == 0>
	<resultMap id="${util.firstLower(beanName)}Mapper" type="${util.type(beanName)}">
		<#list table.columnList as column>
		<#if table.primaryKey.column?exists && table.primaryKey.column.name == column.name>
		<id property="${column.property}" column="${aliasColumnPrefix}${column.name}"/>
		<#else>
		<result property="${column.property}" column="${aliasColumnPrefix}${column.name}"/>
		</#if>
		</#list>
	</resultMap>

	</#if>
	<#-------------------------------------多对一时，1+n查询one------------------------------------------------------------------------>
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#list table.foreignKeys as foreignKey>
	<#if util.isCreateName(table, "getWith${util.firstUpper(foreignKey.column.property)}By") && !foreignKey.table.middle>
	<resultMap id="selectWith${util.firstUpper(foreignKey.column.property)}Mapper" type="${util.type(beanName)}" extends="baseMapper">
		<#list table.columnList as column>
		<#if column.foreign>
		<#if column.name != foreignKey.columnName>
		<association property="${column.property}" javaType="${util.type(util.firstUpper(column.foreignKey.foreignTable.javaName))}">
			<id property="${column.foreignKey.foreignColumn.property}" column="${aliasColumnPrefix}${column.name}"/>
	  	</association>
		<#else>
		<association property="${column.property}" column="${aliasColumnPrefix}${column.name}" javaType="${util.type(util.firstUpper(foreignKey.foreignTable.javaName))}" select="${baseNamespace}.${util.firstUpper(foreignKey.foreignTable.javaName)}Dao.getById"/>
		</#if>
		</#if>
		</#list>
	</resultMap>
	</#if>

	</#list>
	<#-------------------------------------多对一时，1+n查询all------------------------------------------------------------------------>
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreateName(table, "getWithAssociatesBy")>
	<resultMap id="selectWithAssociatesMapper" type="${util.type(beanName)}" extends="baseMapper">
		<#list table.columnList as column>
		<#if column.foreign && column.foreignKey.foreignTable.create>
		<association property="${column.property}" column="${aliasColumnPrefix}${column.name}" javaType="${util.type(util.firstUpper(column.foreignKey.foreignTable.javaName))}" select="${baseNamespace}.${util.firstUpper(column.foreignKey.foreignTable.javaName)}Dao.getById"/>
		</#if>
		</#list>
	</resultMap>

	</#if>
	</#if>
	<#-------------------------------------多对一时，join查询one---------------------------------------------------------------->
	<#list table.foreignKeys as foreignKey>
	<#if util.isCreateName(table, "getJoin${util.firstUpper(foreignKey.column.property)}By")>
	<resultMap id="selectJoin${util.firstUpper(foreignKey.column.property)}Mapper" type="${util.type(beanName)}" extends="baseMapper">
		<#list table.columnList as column>
		<#if column.foreign && column.foreignKey.foreignTable.create>
		<#if column.name != foreignKey.columnName>
		<association property="${column.property}" javaType="${util.type(util.firstUpper(column.foreignKey.foreignTable.javaName))}">
			<id property="${column.foreignKey.foreignColumn.property}" column="${aliasColumnPrefix}${column.name}"/>
	  	</association>
		<#else>
		<association property="${column.property}" resultMap="${baseNamespace}.${util.firstUpper(foreignKey.foreignTable.javaName)}Dao.${foreignKey.foreignTable.javaName}Mapper"/>
		</#if>
		</#if>
		</#list>
	</resultMap>

	</#if>
	</#list>
	<#-------------------------------------多对一时，join查询all------------------------------------------------------------------>
	<#if table.foreignKeys?size gt 0 && util.isCreate(table, "findJoinBy")>
	<resultMap id="selectJoinAssociatesMapper" type="${util.type(beanName)}" extends="baseMapper">
		<#list table.columnList as column>
		<#if column.foreign && column.foreignKey.foreignTable.create>
		<association property="${column.property}" resultMap="${baseNamespace}.${util.firstUpper(column.foreignKey.foreignTable.javaName)}Dao.${util.firstLower(column.foreignKey.foreignTable.javaName)}Mapper"/>
		</#if>
		</#list>
	</resultMap>

	</#if>
	</#if>
	<#-------------------------------------一对多时，join查询one------------------------------------------------------------------>
	<#if table.many?? && table.many?size gt 0 >
	<#list table.many as manyOne>
	<#if util.isCreateName(table, "getJoin${util.firstUpper(manyOne.table.javaName)}By") &&  manyOne.table.create>
	<resultMap id="selectJoin${util.firstUpper(manyOne.table.javaName)}Mapper" type="${util.type(beanName)}" extends="${util.firstLower(beanName)}Mapper">
		<collection property="${manyOne.table.javaName}s" ofType="${util.type(util.firstUpper(manyOne.table.javaName))}"
			resultMap="${baseNamespace}.${util.firstUpper(manyOne.table.javaName)}Dao.${util.firstLower(manyOne.table.javaName)}Mapper">

		</collection>
	</resultMap>
	</#if>

	</#list>
	</#if>
	<#-------------------------------------一对多时，join查询all------------------------------------------------------------------>
	<#if table.many?? && table.many?size gt 1 >
	<#if util.isCreateName(table, "getJoinAll") >
	<resultMap id="selectJoinAllMapper" type="${util.type(beanName)}" extends="${util.firstLower(beanName)}Mapper">
		<#list table.many as manyOne>
		<#if manyOne.table.create>
		<collection property="${manyOne.table.javaName}s" ofType="${util.type(util.firstUpper(manyOne.table.javaName))}"
			resultMap="${baseNamespace}.${util.firstUpper(manyOne.table.javaName)}Dao.${util.firstLower(manyOne.table.javaName)}Mapper">

		</collection>
		</#if>
		</#list>
	</resultMap>

	</#if>
	</#if>
	<#-------------------------------------多对多时，join查询one------------------------------------------------------------------>
	<#if table.manyToManys?? && table.manyToManys?size gt 0 >
	<#list table.manyToManys as mm>
	<#if util.isCreateName(mm.primaryTable, "getMany" + util.firstUpper(mm.secondTable.javaName) + "sBy")>
	<resultMap id="selectMany${util.firstUpper(mm.secondTable.javaName)}sMapper" type="${util.type(beanName)}" extends="${util.firstLower(beanName)}Mapper">
		<collection property="${mm.secondTable.javaName}s" ofType="${util.type(util.firstUpper(mm.secondTable.javaName))}"
			resultMap="${baseNamespace}.${util.firstUpper(mm.secondTable.javaName)}Dao.${util.firstLower(mm.secondTable.javaName)}Mapper">

		</collection>
	</resultMap>
	</#if>
	</#list>
	</#if>
	<#----------------------------select---------------------------------->
	<sql id="selectColumns">
		<#list table.columnList as column>
		${aliasQueryPrefix}${column.name}<#if aliasColumnPrefix != ''>  ${aliasColumnPrefix}${column.name}</#if><#if column_has_next>,</#if>
		</#list>
	</sql>

	<#----------------------------defaultDyamicWhere---------------------------------->
	<#if util.isCreateNames(table, "findJoinBy", "find") >
	<sql id="defaultDyamicWhere">
		<#list table.columnList as column>
		<#if !column.foreign>
		<#if column.javaType == 'Date'>
		<if test="${column.property} != null"> AND ${aliasQueryPrefix}${column.name} &gt;= ${r"#{"}${column.property}${r"}"}</if>
		<if test="start${util.firstUpper(column.property)} != null"> AND ${aliasQueryPrefix}${column.name} &gt;= ${r"#{"}start${util.firstUpper(column.property)}${r"}"}</if>
		<if test="end${util.firstUpper(column.property)} != null"> AND ${aliasQueryPrefix}${column.name} &lt;= ${r"#{"}end${util.firstUpper(column.property)}${r"}"}</if>
		<#elseif column.javaType == 'String'>
		<if test="${column.property} != null and ${column.property} != ''"> AND ${aliasQueryPrefix}${column.name} = ${r"#{"}${column.property}${r"}"}</if>
		<#else>
		<if test="${column.property} != null"> AND ${aliasQueryPrefix}${column.name} = ${r"#{"}${column.property}${r"}"}</if>
		</#if>
		</#if>
		</#list>

		<#list table.columnList as column>
		<#if column.foreign>
		<if test="${column.property} != null and ${column.property}.${column.foreignKey.foreignColumn.property} != null"> AND ${aliasQueryPrefix}${column.name} = ${r"#{"}${column.property}.${column.foreignKey.foreignColumn.property}${r"}"}</if>
		</#if>
		</#list>
	</sql>

	</#if>
	<#----------------------------joinLeftWhere---------------------------------->
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if util.isCreate(table, "findJoinBy") >
	<sql id="joinLeftWhere">
		<#list table.foreignKeys as foreignKey>
		<if test="${foreignKey.column.property} != null">
			<#list foreignKey.foreignTable.columnList as fcolumn>
			<if test="${foreignKey.column.property}.${fcolumn.property} != null"> AND ${foreignKey.foreignTable.alias}.${fcolumn.name} = ${r"#{"}${foreignKey.column.property}.${fcolumn.property}${r"}"}</if>
			</#list>
		</if>
		</#list>
	</sql>
	</#if>
	</#if>
</mapper>
