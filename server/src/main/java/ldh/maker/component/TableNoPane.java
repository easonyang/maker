package ldh.maker.component;

import com.google.gson.Gson;
import com.jfoenix.controls.JFXCheckBox;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.db.TableNoDb;
import ldh.maker.db.TableViewDb;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.vo.TableNo;
import ldh.maker.vo.TableViewData;
import ldh.maker.vo.TreeNode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ldh123 on 2018/5/5.
 */
public class TableNoPane extends BaseSettingTabPane {

    private Map<String, CheckBox> checkBoxMap = new HashMap<>();
    private TableNo tableNo = null;

    public TableNoPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

    protected String getTitle() {
        return "哪些表不需要生成代码，就勾选";
    }

    protected Node initBody() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.getStyleClass().add("round-border");
        scrollPane.setPadding(new Insets(5, 5, 5, 5));

        FlowPane gridPane = new FlowPane();
        gridPane.setHgap(5);
        gridPane.setVgap(10);

        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        for(Map.Entry<String, Table> entry : tableInfo.getTables().entrySet()) {
            Table table = entry.getValue();
            if (table.getPrimaryKey() == null) continue;
            try {
                CheckBox checkBox = new JFXCheckBox(table.getName());
                checkBox.setTooltip(new Tooltip(table.getName()));
                checkBox.setPrefWidth(160);
                checkBoxMap.put(table.getName(), checkBox);
                gridPane.getChildren().addAll(checkBox);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        scrollPane.setContent(gridPane);

        if (tableNo != null) {
            for (String tableName : tableNo.getTableNoes()) {
                if (checkBoxMap.get(tableName) != null) {
                    checkBoxMap.get(tableName).setSelected(true);
                }
            }
        }

        return scrollPane;
    }

    protected void save(ActionEvent event) {
        if(!check()) return;
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        List<String> tableNoes = new ArrayList<>();
        for(Map.Entry<String, CheckBox> entry : checkBoxMap.entrySet()) {
            Table table = tableInfo.getTable(entry.getKey());
            if (entry.getValue().isSelected()) {
                table.setCreate(false);
                tableNoes.add(table.getName());
            }
        }
        new Thread(new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                if (tableNo == null) tableNo = new TableNo();
                Gson gson = new Gson();
                String content = gson.toJson(tableNoes);
                tableNo.setContent(content);
                tableNo.setTreeNodeId(treeItem.getValue().getParent().getId());
                tableNo.setDbName(dbName);
                TableNoDb.save(tableNo);
                return null;
            }
        }).start();
    }

    protected boolean check() {
        return true;
    }

    protected void reset(ActionEvent event) {
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        for(Map.Entry<String, CheckBox> entry : checkBoxMap.entrySet()) {
            entry.getValue().setSelected(tableInfo.getTable(entry.getKey()).isCreate());
        }
    }

    protected void clean(ActionEvent event) {
        for(Map.Entry<String, CheckBox> entry : checkBoxMap.entrySet()) {
            entry.getValue().setSelected(false);
        }
    }

    @Override
    public boolean isLoadEnd() {
        try {
            tableNo = TableNoDb.loadData(treeItem.getValue().getParent(), dbName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
