package ldh.maker;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ldh.maker.util.FileUtil;
import ldh.maker.util.UiUtil;

import java.io.File;
import java.sql.*;

/**
 * Created by ldh on 2017/2/18.
 */
public abstract class ServerMain extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        UiUtil.STAGE = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Main.css");
        scene.getStylesheets().add("/styles/bootstrapfx.css");
        scene.getStylesheets().add("/styles/java-keywords.css");
        scene.getStylesheets().add("/styles/xml-highlighting.css");
        scene.getStylesheets().add(ServerMain.class.getResource("/styles/jfoenix-components.css").toExternalForm());
//        scene.getStylesheets().add(ServerMain.class.getResource("/styles/jfoenix-design.css").toExternalForm());

        stage.setTitle(getTitle());
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(e->{
            try {
                UiUtil.H2CONN.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });

        preHandle();
    }

    protected abstract void preHandle();

    protected String getTitle() {
        return "微服务端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }

    public static void startDb(String dbName) throws Exception {
//        Server server = Server.createTcpServer(args).start();
        dbName = dbName == null ? "db" : dbName;
        Class.forName("org.h2.Driver");
        String file = FileUtil.getSourceRoot() + "/data";
        File f = new File(file);
        while(!f.exists()) {
            f.mkdir();
        }
        file += "/" + dbName;
//        file = "E:\\logs\\maker";
        Connection conn = DriverManager.getConnection("jdbc:h2:" + file, "sa", "");
        initDb(conn);
        UiUtil.H2CONN = conn;
    }

    private static void initDb(Connection conn) throws Exception {
        String sql = "select count(*) from tree_node";
        Statement statement = conn.createStatement();
        boolean hasExist = true;
        try {
//            deleteTables(statement, "tree_node", "db_info");
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {

            }
        } catch (Exception e) {
            hasExist = false;
        }
        if (hasExist) {
            statement.close();
            return;
        }
        sql = FileUtil.loadJarFile("/data.sql");
        System.out.println("sql:::::::" + sql);
        statement.execute(sql);
        statement.close();
    }

    public static void deleteTables(Statement statement, String... tables) throws Exception {
        for (String table : tables) {
            statement.execute("DROP table " + table);
        }
    }
}

