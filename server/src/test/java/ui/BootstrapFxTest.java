package ui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.scene.layout.Panel;

/**
 * Created by ldh123 on 2018/6/9.
 */
public class BootstrapFxTest extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Panel panel = new Panel("This is the title");
        panel.getStyleClass().add("panel-primary");
        BorderPane content = new BorderPane();
        content.setPadding(new Insets(20));
        Button button = new Button("Hello BootstrapFX");
        button.getStyleClass().setAll("btn","btn-danger");
        content.setCenter(button);

        ComboBox<String> checkBox = new ComboBox();
        checkBox.getItems().addAll("sadfadf", "dsfasdfas", "dsfasfa");

        TextField textField = new TextField("1111");

        ProgressBar pb = new ProgressBar();
        pb.setMaxWidth(10);
        Label label = new Label("end");

        HBox hBox = new HBox(checkBox, textField, pb, label);
        content.setTop(hBox);


        panel.setBody(content);

        Scene scene = new Scene(panel);
        scene.getStylesheets().add("bootstrapfx.css");
        scene.getStylesheets().add("/css/checkbox.css");
        primaryStage.setTitle("BootstrapFX");
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.show();
    }
}
